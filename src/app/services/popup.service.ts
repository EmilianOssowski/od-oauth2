import { Injectable } from '@angular/core';
import {MessageService} from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(private message: MessageService) { }

  addInfo( detail, summary = 'Informacja', life = 2000){
    this.message.add({ severity:'info', summary: summary, detail:detail, life:life})
  }
  addError(  detail, summary = 'Wystąpił błąd', life = 2000){
    this.message.add({ severity:'error', summary: summary, detail:detail, life:life})
  }
}
