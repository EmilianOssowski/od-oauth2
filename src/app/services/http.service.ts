import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  url: string = 'http://localhost:8080';
  basicUsername: string = 'www-client';
  basicPassword: string = 'dBaMF9ep';
  basicAuthorization: string = 'Basic d3d3LWNsaWVudDpkQmFNRjllcA==';

  oauthToken = '/oauth/token';
  userEndpoint = '/user';
  registerEndpoint = '/register';

  constructor(private http: HttpClient, private popup : PopupService) { }

  login(username: string, password: string)  {
    let headers = new HttpHeaders({
      'Authorization': this.basicAuthorization,
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    body.set('grant_type', 'password');
    let qs = String(body);
    return this.http.post(this.url + this.oauthToken,
      qs, {headers: headers, withCredentials: true}).pipe(catchError(this.handleError));
  }

  getUserDetails(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });
    return this.http.get(this.url+this.userEndpoint, {
      headers : headers})
      .pipe(catchError(this.handleError))
  }

  register(user) {
    console.log(JSON.stringify(user));
    let headers = new HttpHeaders({
      'Authorization': this.basicAuthorization,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.registerEndpoint, JSON.stringify(user), {
      headers: headers,
      withCredentials: true
    }).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if(error.error == 'invalid_token'){
      this.popup.addInfo('Nastąpiło autowylogowanie')
    }
    return throwError(error || "Server error")
  }
}
