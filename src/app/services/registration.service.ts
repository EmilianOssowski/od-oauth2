import { Injectable } from '@angular/core';
import {PopupService} from "./popup.service";
import {HttpService} from "./http.service";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http:  HttpService, private popup : PopupService) { }

  isRegisterButtonDisabled = false;

  register(user){
    this.isRegisterButtonDisabled = true;
    this.http.register(user).subscribe((res :any) =>{
      this.popup.addInfo('Użytkownik został zarejestrowany');
      this.isRegisterButtonDisabled = false
    },error1 => {
      this.popup.addError('Wystąpił błąd przy rejestracji');
      this.isRegisterButtonDisabled = false;
    });

  }
}

