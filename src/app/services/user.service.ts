import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";
import {CookieService} from "ngx-cookie-service";
import {PopupService} from "./popup.service";
import {Router} from "@angular/router";
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  idUser;
  private username : string;
  private firstName : string;
  private lastName : string;
  private mail : string;

  isUserLogged : boolean =false;
  constructor(private http : HttpService, private cookie : CookieService, private popup : PopupService, private router : Router) { }

  isLoginButtonDisabled = false;
  login(username, password) {
    this.isLoginButtonDisabled = true;
    this.http.login(username, password).subscribe((res : any) =>{
      this.cookie.set('access_token', res.access_token, res.expires_in);
      this.isUserLogged = true;
      this.router.navigate(['/mainpage'])
      this.idUser = res.idUser;
      this.isLoginButtonDisabled = false;


    },(error : any) => {
      if(error.error.error_description == 'Bad credentials') {
        this.popup.addError('Błędne dane logowania');
      }
      else {
        this.popup.addError(error.error);
      }
      this.isLoginButtonDisabled = false;
    })
  }
  getUserDetails(){
    this.http.getUserDetails(this.getBearerToken()).subscribe((res : UserModel )=>{
      this.mail = res.mail;
      this.firstName = res.firstName;
      this.lastName = res.lastName;

      this.router.navigate(['/mainpage']);
    },error => {
      this.popup.addError( error.error.error);
    })
  }

  logout(){
    this.isUserLogged = false;
  }
  checkCredentials(username, password){
    if(username != undefined) {
      this.username = username;
      return true;
    }
    else{
      return false;
    }
  }
  getBearerToken(){
    return this.cookie.get('access_token')
  }
}
