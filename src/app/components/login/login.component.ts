import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username : string;
  password : string;

  display : boolean = true;
  constructor(private router: Router, private user : UserService) { }

  ngOnInit() {

  }

  login(){
    try{
      this.user.login(this.username, this.password)
    }catch (e) {
      console.log("Błąd")
    }
  }

}
