import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {RegistrationService} from "../../services/registration.service";
import {UserModel} from "../../models/user.model";
import {PopupService} from "../../services/popup.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  display : boolean = true;
  constructor(private router : Router, private registration : RegistrationService, private popup : PopupService) { }

  password ='bcrypt';
  confirmPassword = 'bcrypt';
  firstName = 'emil';
  lastName = 'osa';
  mail = 'emilian.ossowski@student.put.poznan.pl';
  ngOnInit() {
  }
  register(){
    if(this.password == this.confirmPassword) {
      let user: UserModel = new UserModel();
      user.mail = this.mail;
      user.password = this.password;
      user.lastName = this.lastName;
      user.firstName = this.firstName;
      this.registration.register(user)
    }else this.popup.addError('Hasła się nie zgadzają')

  }
}
