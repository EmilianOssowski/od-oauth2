import { Component } from '@angular/core';
import {UserService} from "./services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'oauth-implementation';

  constructor(private router : Router, private user: UserService) {
  }

  logout(){
    this.user.logout();
    this.router.navigate(['/login'])
  }
}
